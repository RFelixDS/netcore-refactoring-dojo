﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class AtualizarQualidadeIngressosService : IAtualizarQualidadeService
    {
        public void AtualizarQualidade(Item item)
        {
            item.PrazoValidade--;

            if (item.PrazoValidade < 0)
            {
                item.Qualidade = item.Qualidade - item.Qualidade;
            }
            else if (item.PrazoValidade < 5)
            {
                item.Qualidade += 3;
            }
            else if (item.PrazoValidade < 10)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade += 1;
            }

            item.Qualidade = (item.Qualidade > 50) ? 50 : item.Qualidade;
        }
    }
}
