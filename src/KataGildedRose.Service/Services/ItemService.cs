﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Repositories;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _itemRepository;

        public ItemService()
        {
        }

        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }
        public List<Item> GetItens()
        {
            return _itemRepository.GetItens();
        }
    }
}
