﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class AtualizarQualidadeConjuradoService : IAtualizarQualidadeService
    {
        public void AtualizarQualidade(Item item)
        {
            item.PrazoValidade--;

            if (item.PrazoValidade < 0)
                item.Qualidade -= 4;
            else
                item.Qualidade -= 2;

            item.Qualidade = item.Qualidade < 0 ? 0 : item.Qualidade;
        }
    }
}
