﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class AtualizarQualidadeQueijoBrieService : IAtualizarQualidadeService
    {
        public void AtualizarQualidade(Item item)
        {
            item.PrazoValidade--;

            if (item.PrazoValidade < 0)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade++;
            }
            item.Qualidade = (item.Qualidade > 50) ? 50 : item.Qualidade;
        }
    }
}
