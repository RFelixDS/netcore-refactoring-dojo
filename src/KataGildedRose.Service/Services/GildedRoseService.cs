﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Repositories;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class GildedRoseService : IGildedRoseService
    {
        private readonly IAtualizadorEstoqueService _atualizadorEstoqueService;

        public GildedRoseService()
        {
        }

        public GildedRoseService(IAtualizadorEstoqueService atualizadorEstoqueService)
        {
            _atualizadorEstoqueService = atualizadorEstoqueService;
        }

        public List<Item> AtualizarQualidade(int dias, List<Item> Itens)
        {
            if (dias < 0)
                throw new Exception("O numero de dias deve ser um valor não negativo!");
            for(int i = 0; i < dias; i++)
            {
                _atualizadorEstoqueService.AtualizarQualidade(Itens);
            }
            return Itens;
        }
    }
}
