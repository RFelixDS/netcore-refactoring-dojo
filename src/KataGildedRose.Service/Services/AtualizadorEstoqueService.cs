﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KataGildedRose.Service.Services
{
    public class AtualizadorEstoqueService : IAtualizadorEstoqueService
    {
        public AtualizadorEstoqueService()
        {
        }

        public IAtualizarQualidadeService GetTipoItem(Item item)
        {
            return item.Nome switch
            {
                "Queijo Brie Envelhecido" => new AtualizarQualidadeQueijoBrieService(),
                string a when a.Contains("Ingressos") => new AtualizarQualidadeIngressosService(),
                "Dente do Tarrasque" => new AtualizarQualidadeLendarioService(),
                "Sulfuras, a Mão de Ragnaros" => new AtualizarQualidadeLendarioService(),
                string a when a.Contains("Conjurado") => new AtualizarQualidadeConjuradoService(),
                _ => new AtualizarQualidadeItemComumService(),
            };
        }

        public List<Item> AtualizarQualidade(List<Item> Itens)
        {
            foreach (Item item in Itens)
            {
                var Atualizar = GetTipoItem(item);
                Atualizar.AtualizarQualidade(item);
            }
            return Itens;
        }
    }
}
