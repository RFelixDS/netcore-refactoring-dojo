﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Infra.Data.Repository
{
	public class ItemRepository : IItemRepository
	{
        public ItemRepository()
        {

        }
        readonly List<Item> itens = new List<Item>{
				new Item {Nome = "Corselete +5 DEX", PrazoValidade = 10, Qualidade = 20},
				new Item {Nome = "Queijo Brie Envelhecido", PrazoValidade = 2, Qualidade = 0},
				new Item {Nome = "Elixir do Mangusto", PrazoValidade = 5, Qualidade = 7},
				new Item {Nome = "Dente do Tarrasque", PrazoValidade = 0, Qualidade = 80},
				new Item {Nome = "Dente do Tarrasque", PrazoValidade = -1, Qualidade = 80},
				new Item {Nome = "Ingressos para o concerto do Turisas",PrazoValidade = 15,Qualidade = 20},
				new Item {Nome = "Ingressos para o concerto do Turisas",PrazoValidade = 10,Qualidade = 49},
				new Item {Nome = "Ingressos para o concerto do Turisas",PrazoValidade = 5,Qualidade = 49},
				new Item {Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 6},
				new Item {Nome = "Ingressos para o concerto do Turisas Conjurado", PrazoValidade = 4, Qualidade = 11}
			};
				
		public List<Item> GetItens()
		{
			return itens;
		}
	}
}
