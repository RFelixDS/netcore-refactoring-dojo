﻿using KataGildedRose.Domain;
using KataGildedRose.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace KataGildedRose.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GildedRoseController : ControllerBase
    {
        private readonly IGildedRoseService _gildedRoseService;
        private readonly IItemService _itemService;
        public GildedRoseController(IGildedRoseService gildedRoseService, IItemService itemService)
        {
            _gildedRoseService = gildedRoseService;
            _itemService = itemService;
        }

        /// <summary>
        /// Obter lista de items atualiza por dia
        /// </summary>
        /// <param name="dias">Numero de dias</param>
        /// <response code="200"> A lista de items foi obtida com sucesso.</response>
        /// <response code="400"> Ocorreu um erro ao tentar obter a lista de itens.</response>

        [HttpGet("/api/gildedrose/{dias}")]
        [ProducesResponseType(typeof(List<Item>), 200)]
        [ProducesResponseType(400)]
        public ActionResult<List<Item>> Get(int dias)
        {
            var itens = _itemService.GetItens();

            try
            {
                itens = _gildedRoseService.AtualizarQualidade(dias, itens);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(itens);
        }
    }
}
