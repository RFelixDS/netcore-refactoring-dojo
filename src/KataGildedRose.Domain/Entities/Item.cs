﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Domain
{
    public sealed class Item
    {
        public string Nome { get; set; }

        public int PrazoValidade { get; set; }

        public int Qualidade { get; set; }
    }
}

