﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Domain.Interfaces.Services
{
    public interface IAtualizadorEstoqueService
    {
        List<Item> AtualizarQualidade(List<Item> Itens);
    }
}
