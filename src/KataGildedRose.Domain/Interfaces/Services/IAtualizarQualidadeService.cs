﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Domain.Interfaces.Services
{
    public interface IAtualizarQualidadeService
    {
        public void AtualizarQualidade(Item item);
    }
}
