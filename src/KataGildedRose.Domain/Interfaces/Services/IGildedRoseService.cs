﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Domain.Interfaces.Services
{
    public interface IGildedRoseService
    {
        List<Item> AtualizarQualidade(int dias, List<Item> Itens);
    }
}
