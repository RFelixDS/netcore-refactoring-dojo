﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Domain.Interfaces.Repositories
{
    public interface IItemRepository
    {
        List<Item> GetItens();
    }
}
