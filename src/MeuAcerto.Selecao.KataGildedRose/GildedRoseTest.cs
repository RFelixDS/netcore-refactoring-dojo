﻿using KataGildedRose.Domain.Interfaces.Services;
using KataGildedRose.Infra.Data.Repository;
using KataGildedRose.Service.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        private ItemRepository _itemService;
        private AtualizadorEstoqueService _gildedRoseService;
        public GildedRoseTest()
        {
            _itemService = new ItemRepository();
            _gildedRoseService = new AtualizadorEstoqueService();
        }

        [Fact]
        public void DenteDoTarrasqueNaoPodeSerAtualizado()
        {
            var itens = _itemService.GetItens();
            var itensValoresIniciais = _itemService.GetItens();

            for (var i = 0; i < 31; i++)
            {
                itens = _gildedRoseService.AtualizarQualidade(itens);
            }
            var denteDoTarrasque = itens.Where(i => i.Nome.Equals("Dente do Tarrasque"));
            var denteDoTarrasqueInicial = itensValoresIniciais.Where(i => i.Nome.Equals("Dente do Tarrasque"));

            Assert.Contains(denteDoTarrasque, i => i.Qualidade == denteDoTarrasqueInicial.Select(d => d.Qualidade).FirstOrDefault());
            Assert.Contains(denteDoTarrasque, i => i.PrazoValidade == denteDoTarrasqueInicial.Select(d => d.PrazoValidade).FirstOrDefault());
        }

        [Fact]
        public void NaoPodeExistirQualidadeNegativa()
        {
            var itens = _itemService.GetItens();

            for (var i = 0; i < 31; i++)
            {
                _gildedRoseService.AtualizarQualidade(itens);
            }
            Assert.DoesNotContain(itens, i => i.Qualidade < 0);
        }

        [Fact]
        public void QualidadeDosIgressosForaDaValidadeDeveSerIgualAZero()
        {
            var itens = _itemService.GetItens();

            for (var i = 0; i < 31; i++)
            {
                _gildedRoseService.AtualizarQualidade(itens);
            }

            var ingressos = itens.Where(i => i.Nome.Equals("Ingressos para o concerto do Turisas"));
            Assert.DoesNotContain(ingressos, i => i.PrazoValidade <= 0 && i.Qualidade > 0);
        }


        [Fact]
        public void QualidadeMaxima50()
        {
            var itens = _itemService.GetItens();

            for (var i = 0; i < 31; i++)
            {
                _gildedRoseService.AtualizarQualidade(itens);
            }
            var FiltraItens = itens.Where(i => !i.Nome.Equals("Sulfuras, a Mão de Ragnaros") && !i.Nome.Equals("Dente do Tarrasque"));
            Assert.DoesNotContain(FiltraItens, i => i.Qualidade > 50);
        }        
        

        [Fact]
        public void foo()
        {
            var itens = _itemService.GetItens();
            _gildedRoseService.AtualizarQualidade(itens);
            Assert.Equal("Corselete +5 DEX", itens[0].Nome);
        }
    }
}
