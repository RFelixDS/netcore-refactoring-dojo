﻿using KataGildedRose.Domain.Interfaces.Repositories;
using KataGildedRose.Domain.Interfaces.Services;
using KataGildedRose.Infra.Data.Repository;
using KataGildedRose.Service.Services;
using Microsoft.Extensions.DependencyInjection;


namespace KataGildedRose.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureService
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IGildedRoseService, GildedRoseService>();
            serviceCollection.AddScoped<IItemService, ItemService>();
            serviceCollection.AddScoped<IAtualizadorEstoqueService, AtualizadorEstoqueService>();
        }
    }
}
