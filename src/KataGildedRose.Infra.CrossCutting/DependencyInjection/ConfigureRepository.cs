﻿using KataGildedRose.Domain.Interfaces.Repositories;
using KataGildedRose.Infra.Data.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace KataGildedRose.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureRepository
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IItemRepository, ItemRepository>();
        }
    }
}
